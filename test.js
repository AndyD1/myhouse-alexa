//var config = require('./config');
var helper = require('./helper');
var HA = require('./lib/openhab');

console.log(helper.getMode('szene', 'musik'));
console.log(helper.getMode('szene', 'fernsehen'));
console.log(helper.getMode('szene', 'guten morgen'));


// console.log(helper.formatString("{0} is a {1}", "Michael", "Guy"));

// openhab.getItem('gWindowsDoorsOpen', (err, json) => {
//     console.log("** gWindowsDoorsOpen **");
//     var itemList = flattenItems(json.members);
//     console.log(itemList);
// });


// get all open windows/doors
HA.getItem('gWindowsDoorsOpen', function (err, json) {
    if (!err) {
        var open = helper.flattenItems(json.members).filter((itm) => { return itm.type === 'Contact' && itm.state !== 'CLOSED' });

        // no doors open => check ventilation
        if (open.length == 0) {
            // check for doors/windows in ventilation
            HA.getItem('gWindowsDoorsVent', function (err, json) {
                if (!err) {
                    var ventilation = helper.flattenItems(json.members).filter((itm) => { return itm.type === 'Contact' && itm.state !== 'CLOSED' });

                    // all doors and windows closed !
                    if (ventilation.length==0) {
                        console.log("Alles ok! Fenster und Türen sind fest verschlossen.");
                    }
                    // only doors and windows in ventilatiuon
                    else {
                        console.log(`Alles zu, aber ` )  
                         for(var dw of ventilation) {
                           console.log(helper.translateItem(dw  .name) + " ist auf lüftung");            
                        }
                    }


                }
            });

        }
        // doors/windows open
        else {
            var text = "Achtung !"
            console.log("Achtung!");
            for(var dw of open) {
                console.log(helper.translateItem(dw.name) + " ist offen");            
            }
        }


        //replyWith(`Aktuell sind ${open.length} Fenster offen!`, response);
    }
});

