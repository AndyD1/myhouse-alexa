/* Default Alexa HA configuration file.  Copy this to 'config.js' and adapt as needed! */
var config = {};

/******************************** GENERAL CONFIGURATION *****************************************/
// Name of the application/skill, which does not need to match the skills 'Invocation Name',
// Defines both the Alexa-App-Server endpoint and the skills own name used in cards.
config.applicationName = 'SmartHomeInning';

// AWS ASK applicationId, resembles 'amzn1.echo-sdk-ams.app.[your-unique-value-here]'
config.applicationId = 'amzn1.ask.skill.261380d1-40c8-4206-97b1-091bd8b91e70';

// AWS ASK userID, resembles 'amzn1.echo-sdk-account.[your-unique-value-here]'
config.userId = 'amzn1.echo-sdk-account.XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX';

// AWS ASK password, randomly generated password to be included in the Skill's endpoint URL (i.e. '?password=XXXXXXXXXXXX')
config.password = 'XXXXXXXXXXXX';

// Greeting, when saying 'Alexa, open...'
config.greeting = "Hallo! Hier ist Sarah.";
config.greetingHelp = "Wie kann ich Dir helfen?";
config.sessionEnd = "Bis zum nächsten mal.";
config.noIntentFound = "Ich weiss nicht genau, was du möchtest. Kannst Du es bitte wiederholen...";
config.genericError = "Ohhh, das sollte nicht passieren! ";

config.setStateError = "Mist, das sollte nicht passieren";


// URL of an MP3 to play when opening the skill (optional; must be MPEG v2 codec / 48kbps bit rate / 16000 sample rate)
//config.chime = "\ <audio src=\"https://MY_FQDN/files/chime.mp3\" /> ";

// TODO Array of IPs permitted to call the application endpoint
//config.allowed_ips = {'AWS':'72.21.217.159', 'Desktop':'192.168.1.5'};

// Wolfram Alpha API key (Optional, used for Research Intent), resembles 'XXXXXX-XXXXXXXXXX' - https://developer.wolframalpha.com/portal/apisignup.html
config.wolframAppId = 'XXXXXX-XXXXXXXXXX';

/***************************** HOME AUTOMATION CONFIGURATION ********************************************/
// Choose which Home Automation Controller you are using
// TODO Only 'OpenHAB' is supported at this time.  SmartThings, INSTEON, Vera, etc support is planned.
config.HA_name = 'OpenHAB';

// HA server protocol (http/https)
config.HA_protocol = 'https';

// HA server username
config.HA_user = 'info@01tec.de';

// HA server password
config.HA_password = 'man100itou';

// HA server FQDN/IP
config.HA_host = 'myopenhab.org';

// HA server port
config.HA_port = '443';

// HA server URL with credentials - i.e. 'http(s)://USERNAME:PASSWORD@HA_SERVER_IP:HA_SERVER_PORT' (constructed from above variables)
config.HA_server = config.HA_protocol + '://' + config.HA_user + ':' + config.HA_password + '@' + config.HA_host + ':' + config.HA_port;
//config.HA_server = config.HA_protocol + '://' + config.HA_host + ':' + config.HA_port;

// TODO - HA Switch Item which toggles ECHO request handling ON/OFF
//config.HA_item_switch = 'ECHO_Switch';

// TODO HA String Item which stores the ASK requestID
//config.HA_item_requestId = 'ECHO_RequestId';

//HA Switch Item which determines if the latest request been processed
config.HA_item_processed = 'ECHO_Processed';

// HA String Item which stores the voice command
config.HA_item_voicecmd = 'ECHO_VoiceCMD';

// HA String Item which stores the servers response/answer, to be spoken by ECHO
config.HA_item_answer = 'ECHO_Answer';


// Array of available rooms that HA has devices in, used for validations & re-prompts
//config.HA_locations = ['alles','haus','büro','essen','wohnen','küche'];

// Item configuration - itemType / Location / itemName mappings
config.synonyms = {
    'no-location': 'haus',
    'music': 'musik,multiroom',

    'lights': 'licht,lichter,spots',
    'rgbled-master': 'master',
    'rgbled': 'farbe,lichtfarbe',
    'wall-light': 'fassadenlicht',
    'pendant': 'hängelampe,hängeleuchte',
    'cook-light': 'kochlicht',
    'coffe-maker': 'kaffeemaschine',
    'sauna-stove': 'saunaofen,ofen',
    'fb': 'fensterband, fensterlicht',
    'fb-master': 'master fensterband,master fensterlicht',
    'fb-rgb': 'farbe fensterband,farbe fensterlicht, lichtfarbe fensterband,lichtfarbe fensterlicht',
    'br': 'brüstung,brüstungslicht',
    'br-master': 'master brüstung',
    'br-rgb': 'farbe brüstung,lichtfarbe brüstung',
    'light-band': 'lichtband,lichtbänder,lichtleiste',
    'fan': 'lüftung,lüfter',

    'sauna': 'sauna,saunabänke,saunabank',
    'sauna-bu': 'saunabank oben',
    'sauna-bl': 'saunabank unten',
    'spa': 'wellness,wellnessbereich, wellness bereich',
    'spa-shower': 'wellnessdusche, wellness dusche',
    'cinema': 'heimkino,kino',
    'office': 'büro, arbeitszimmer',
    'heating': 'heizung,heizraum',
    'technics': 'serverraum,technik',
    'store': 'abstellkammer,abstellraum',
    'laundry': 'hauswirtschaft',
    'corridorUG': 'flur unten',


    'living': 'wohnen,wohnzimmer',
    'eat': 'essen,esszimmer',
    'kitchen': 'küche',
    'coffe-bar': 'kaffebar',
    'kitchen-block': 'küchenblock',
    'sink': 'spüle',
    'toilet': 'wc,klo',
    'corridorEG': 'eingang,eingangsbereich,flur',
    'stairs': 'treppe, treppe e.g,treppe e.g.,treppe eg,treppe erdgeschoß',
    'stairs-ug': 'treppe u.g,treppe u.g.,treppe ug,treppe unten,treppe untergeschoß',

    'sleep': 'schlafzimmer',
    'bath1': 'hauptbad,bad',
    'bath1-shower': 'dusche,hauptdusche,baddusche',
    'bath2': 'gästebad',
    'bath2-shower': 'gästedusche',
    'guest1': 'gast eins, gast rechts',
    'guest2': 'gast zwei, gast links',
    'corridorOG': 'flur oben,gallerie',

    'garden': 'garten',

    //    'garage': 'garage',
    'terrace': 'terasse',
    'terraceDT': 'dachterasse',
    'balcony': 'balkon',
    'window': 'giebel,fensterfront',
    'entrance': 'vordach',

    'lightwell-master': 'master lichtschacht',
    'lightwell-rgb': 'farbe lichtschacht, lichtfarbe lichtschacht',

    'outdoor': 'außen, außenbereich, draußen',

    'ug': 'untergeschoß',
    'eg': 'erdgeschoß',
    'og': 'obergeschoß',
};
config.item = {
    // for this items, we don*t check the current state !
    dontCheckState: [
        'Light_Unlock_Motion_All',
        'Light_Unlock_Motion_U_All',
        'Light_Unlock_Motion_E_All',
        'Light_Unlock_Motion_O_All',
        'MultiRoom_Pwr_All'
    ],

    'sauna-stove': {   // saunaofen,ofen
        'no-location': 'Plug_U01_Sauna_Stove',
        'sauna': 'Plug_U01_Sauna_Stove'
    },

    'coffe-maker': {   // kaffeemaschine
        'no-location': 'Plug_E03_Kitchen_Coffe',
        'kitchen': 'Plug_E03_Kitchen_Coffe'
    },

    'fb': {  // fensterband, fensterlicht
        'no-location': 'Light_E_FB_All_W',
        'kitchen': 'Light_E03_Kitchen_FB_A_W',
        'coffe-bar': 'Light_E03_Kitchen_FB_B_W',
        'eat': 'Light_E02_Eat_FB_C_W',
    },
    'fb-master': {  // master fensterband,farblicht fensterlicht
        'no-location': 'Light_E_FB_All_RGB_M',
        'kitchen': 'Light_E03_Kitchen_FB_A_RGB_M',
        'coffe-bar': 'Light_E03_Kitchen_FB_B_RGB_M',
        'eat': 'Light_E02_Eat_FB_C_RGB_M',
    },
    'fb-rgb': { // farbe fensterband,farbe fensterlicht, lichtfarbe fensterband,lichtfarbe fensterlicht
        'no-location': 'Light_E_FB_All_RGB',
        'kitchen': 'Light_E03_Kitchen_BR_A_RGB',
        'coffe-bar': 'Light_E03_Kitchen_BR_B_RGB',
        'eat': 'Light_E02_Eat_BR_C_RGB',
    },

    'br': { //brüstung,brüstungslicht
        'no-location': 'Light_E_BR_All_W',
        'kitchen': 'Light_E03_Kitchen_BR_A_W',
        'coffe-bar': 'Light_E03_Kitchen_BR_B_W',
        'eat': 'Light_E02_Eat_BR_C_W',
    },
    'br-master': { // master brüstung
        'no-location': 'Light_E_BR_All_RGB_M',
        'kitchen': 'Light_E03_Kitchen_BR_A_RGB_M',
        'coffe-bar': 'Light_E03_Kitchen_BR_B_RGB_M',
        'eat': 'Light_E02_Eat_BR_C_RGB_M',
    },
    'br-rgb': { // farbe brüstung,lichtfarbe brüstung
        'no-location': 'Light_E_BR_All_RGB',
        'kitchen': 'Light_E03_Kitchen_BR_A_RGB',
        'coffe-bar': 'Light_E03_Kitchen_BR_B_RGB',
        'eat': 'Light_E02_Eat_BR_C_RGB',
    },

    'lightwell-master': {   // master lichtschacht
        'no-location': 'Light_LightWell_All_RGB_M',
        'laundry': 'Light_U04_LightWell_RGB_M',
        'corridorEG': 'Light_U05_LightWell_RGB_M',
        'garden': 'Light_U06_LightWell_RGB_M',
    },
    'lightwell-rgb': {   // farbe lichtschacht
        'no-location': 'Light_LightWell_All_RGB',
        'laundry': 'Light_U04_LightWell_RGB',
        'corridorEG': 'Light_U05_LightWell_RGB',
        'garden': 'Light_U06_LightWell_RGB',
    },

    'cook-light': { // kochlicht   
        'no-location': 'Light_E03_Kitchen_Cook',
        'kitchen': 'Light_E03_Kitchen_Cook'
    },

    'wall-light': { // fassadenlicht
        'no-location': 'Light_U10_Garden_Wall_All',
        'garden': 'Light_U10_Garden_Wall_All',
    },

    'pendant': {    // hängelampe
        'no-location': 'Light_E02_Eat_Pendant',
        'office': 'Light_U03_Office_Pendant',
        'eat': 'Light_E02_Eat_Pendant',
    },

    'light-band': { // lichtband,lichtbänder,lichtleiste
        'kitchen': 'Light_E03_Kitchen_LB_All',
        'coffe-bar': 'Light_E03_Kitchen_LB2',
        'sink': 'Light_E03_Kitchen_LB1',
        'corridorEG': 'Light_E04_Corridor_LB_W',
        'corridorOG': 'Light_O06_Corridor_LB1_W',
        'entrance': 'Light_E08_Entrance_Ceiling_W',
    },



    'lights': { // licht
        'no-location': 'Light_Unlock_Motion_All',
        'ug': 'Light_Unlock_Motion_U_All',
        'eg': 'Light_Unlock_Motion_E_All',
        'og': 'Light_Unlock_Motion_O_All',

        'sauna': 'Light_U01_Sauna_Bench_back_W',
        'spa': 'Light_U02_Spa_All',
        'spa-shower': 'Light_U02_Spa_Shower_W',

        'office': 'Light_U03_Office_All',
        'laundry': 'Light_U04_Laundry_All',
        'heating': 'Light_U05_Heating_All',
        'cinema': 'Light_U06_Cinema_All',
        'technics': 'Light_U08_Technics_All',
        'store': 'Light_U11_Store_All',
        'corridorUG': 'Light_U09_Corridor_All',

        'living': 'Light_E01_Living_All',
        'eat': 'Light_E02_Eat_All',
        'kitchen': 'Light_E03_Kitchen_Wellcome',
        'coffe-bar': 'Light_E03_Kitchen_LB2',
        'sink': 'Light_E03_Kitchen_LB1',
        'corridorEG': 'Light_E04_Corridor_All',
        'toilet': 'Light_E06_Toilet_L03_L04',

        'sleep': 'Light_O01_Sleep_Pendant',
        'bath1': 'Light_O02_Bath1_LB2_W',
        'bath1-shower': 'Light_O02_Bath1_LB1_W',
        'bath2': 'Light_O03_Bath2_LB2_W',
        'bath2-shower': 'Light_O03_Bath2_LB1_W',

        'guest1': 'Light_O05_Guest1_LB1_W',
        'guest2': 'Light_O04_Guest2_LB1_W',

        'corridorOG': 'Light_O06_Corridor_Pendant',

        'entrance': 'Light_E08_Entrance_Ceiling_W',
        'garden': 'Light_U10_Garden_Spots_All',
    },

    'rgbled-master': {  // master
        'sauna-bu': 'Light_U01_Sauna_Bench_upper_RGB_M',
        'sauna-bl': 'Light_U01_Sauna_Bench_lower_RGB_M',
        'sauna': 'Light_U01_Sauna_Bench_All_RGB_M',
        'spa': 'Light_U02_Spa_LB1_RGB_M',
        'spa-shower': 'Light_U02_Spa_Shower_RGB_M',

        'kitchen-block': 'Light_E03_Kitchen_Block_RGB_M',
        'corridorEG': 'Light_E04_Corridor_LB_RGB_M',

        'bath1': 'Light_O02_Bath1_LB1_RGB_M',
        'bath1-shower': 'Light_O02_Bath1_LB1_RGB_M',
        'bath2': 'Light_O03_Bath2_LB1_RGB_M',
        'bath2-shower': 'Light_O03_Bath2_LB1_RGB_M',

        'corridorOG': 'Light_O06_Corridor_RGB_M',

        'stairs': 'Light_StairsEG_All_RGB_M',
        'stairs-ug': 'Light_StairsUG_All_RGB_M',

        'entrance': 'Light_E08_Entrance_Ceiling_RGB_M',
        'window': 'Light_O08_Window_RGB_M',

    },

    'rgbled': { // farbe,lichtfarbe
        'sauna-bu': 'Light_U01_Sauna_Bench_upper_RGB',
        'sauna-bl': 'Light_U01_Sauna_Bench_lower_RGB',
        'sauna': 'Light_U01_Sauna_Bench_All_RGB',
        'spa': 'Light_U02_Spa_LB1_RGB',
        'spa-shower': 'Light_U02_Spa_Shower_RGB',

        'kichen-block': 'Light_E03_Kitchen_Block_RGB',
        'corridorEG': 'Light_E04_Corridor_LB_RGB',

        'bath1': 'Light_O02_Bath1_LB1_RGB',
        'bath1-shower': 'Light_O02_Bath1_LB1_RGB',
        'bath2': 'Light_O03_Bath2_LB1_RGB',
        'bath2-shower': 'Light_O03_Bath2_LB1_RGB',

        'corridorOG': 'Light_O06_Corridor_RGB',

        'stairs': 'Light_StairsEG_All_RGB',
        'stairs-ug': 'Light_StairsUG_All_RGB',

        'entrance': 'Light_E08_Entrance_Ceiling_RGB',
        'window': 'Light_O08_Window_RGB',

    },

    'motion': {
        'spa': 'Motion_Blocked_U02_Spa',
        'office': 'Motion_Blocked_U03_Office',
        'laundry': 'Motion_Blocked_U04_Laundry',
        'cinema': 'Motion_Blocked_U06_Cinema',
        'corridorUG': 'gMotionBlockedCorridorU',

        'living': 'Motion_Blocked_E01_Living',
        'eat': 'gMotionBlockedEat',
        'kitchen': 'Motion_Blocked_E03_Kitchen',
        'corridorEG': 'Motion_Blocked_E04_Corridor',
        'corridorOG': 'gMotionBlockedCorridorO',
        'entrance': 'Motion_Blocked_E08_Entrance'
    },

    'fan': {
        'spa-shower': 'Vent_U02_Spa_Fan',
        'toilet': 'Vent_E06_Toilet_Fan',
    },

    'music': {
        'no-location': 'MultiRoom_Pwr_All',
        'spa': 'MR_U02_Spa_Pwr_Z7',
        'sauna': 'MR_U01_Sauna_Pwr_Z11',
        'office': 'MR_U03_Office_Pwr_Z12',
        'laundry': 'MR_U04_Laundry_Pwr_Z13',

        'living': 'MR_E01_Living_Pwr_Z15',
        'eat': 'MR_E02_Eat_Pwr_Z10',
        'kitchen': 'MR_E03_Kitchen_Pwr_Z9',

        'sleep': 'MR_O01_Sleep_Pwr_Z6',
        'bath1': 'MR_O02_Bath1_Pwr_Z5',
        'bath2': 'MR_O03_Bath2_Pwr_Z4',
        'guest1': 'MR_O05_Guest1_Pwr_Z1',
        'guest2': 'MR_O04_Guest2_Pwr_Z2',
        'garden': 'MR_U10_Garden_Pwr_Z8',
        'terrace': 'MR_E07_Terrace_Pwr_Z16',
        'terraceDT': 'MR_ODT_Terrace_Pwr_Z3',
        'garage': 'MR_E05_Garage_Pwr_Z14'

    },
    'mode': {
        'szene': 'CNF_SCENE_CallSceneNum_A',
        'security': 'Security_scenens',
        default: 'CNF_SCENE_CallSceneNum_A'
    },
    /*
        'volume': {
            'living room': 'Living_Volume',
            'office': 'Office_Volume',
            default: 'Office_Volume'
        },
        'thermostat': {
            'house': 'HVAC_Target_Temp',
            'home': 'HVAC_Target_Temp',
            default: 'HVAC_Target_Temp'
        },
        'lock': {
            'front door': 'Lock_Front_Door',
            'garage door': 'Lock_Garage_Door',
            default: 'Lock_All_Doors'
        },
        */
};

config.action = {
    'ein': 'ON',
    'an': 'ON',
    'aus': 'OFF'
};

config.rollershuterAction = {
    'up': 'UP',
    'down': 'DOWN'
};

config.blockAction = {
    'sperre': 'ON',
    'entsperre': 'OFF',
    'ein': 'OFF',
    'aus': 'ON'
};

config.translate = {
    'item': {
        'Window_Open_U03_Office': 'Bürofenster',
        'Window_Vent_U03_Office': 'Bürofenster',
        'Window_Open_U04_Laundry': 'Fenster im Hauswirtschaftsraum',
        'Window_Vent_U04_Laundry': 'Fenster im Hauswirtschaftsraum',
        'Window_Open_U05_Heating': 'Fenster im Heizraum',
        'Window_Vent_U05_Heating': 'Fenster im Heizraum',
        'Window_Vent_U06_Cinema': 'Fenster im Heimkino',
        'Window_Vent_U06_Cinema': 'Fenster im Heimkino',
        'Window_Open_U08_Technics': 'Fenster im Serverraum',
        'Window_Vent_U08_Technics': 'Fenster im Serverraum',
        'Window_Open_E03_Kitchen': 'Küchenfenster',
        'Window_Vent_E03_Kitchen': 'Küchenfenster',
        'Window_Open_O02_Bath1': 'Dachfenster im Hauptbad',
        'Window_Open_O03_Bath2': 'Dachfenster im Gästebad',
        'Window_Open_O05_Guest1': 'Fenster im rechten Gästezimmer',
        'Window_Vent_O05_Guest1': 'Fenster im rechten Gästezimmer',
        'Window_Open_O04_Guest2': 'Fenster im linken Gästezimmer',
        'Window_Vent_O04_Guest2': 'Fenster im linken Gästezimmer',
        'Window_Open_O06_Roof_South': 'südliches Giebelfenster',
        'Window_Open_O06_Roof_North': 'nördliches Giebelfenster',
        'Door_Open_U02_Spa': 'Tür im Wellnessbereich',
        'Door_Vent_U02_Spa': 'Tür im Wellnessbereich',
        'Door_Open_E04_Main': 'Eingangstür',
        'Door_Open_E01_Living': 'Terassentür',
        'Door_Vent_E01_Living': 'Terassentür',
        'Door_Open_E01_W_Living': 'Terassentür zum Garten',
        'Door_Vent_E01_W_Living': 'Terassentür zum Garten',
        'Door_Open_O01_Sleep': 'Schlafzimmertür',
        'Door_Vent_O01_Sleep': 'Schlafzimmertür',
        'Door_Open_O05_Guest1': 'Tür im rechten Gästezimmer',
        'Door_Vent_O05_Guest1': 'Tür im rechten Gästezimmer',
    }
};


// Rollershutters
config.shutters = {
    'synonyms': {
        'no-location': 'haus,alle,fensterband',
        'shutter': 'jalousie',
        'slat': 'lamelle',

        'up': 'hoch,öffne,rauf',
        'down': 'schließe,runter',

        'eat-left': 'essen links, esszimmer links',
        'eat-right': 'essen rechts,esszimmer rechts',
        'coffebar': 'kaffebar',
        'kitchen-corner-north': 'küche ecke nord',
        'kitchen-corner-east': 'küche ecke ost',
        'kitchen-east': 'spüle,küche ost',
        'kitchen-window': 'küchenfenster'
    },
    'shutter': {
        'no-location': 'Shutters_E02_E03_All',
        'eat-left': 'Shutters_E02_10_Eat_Left',
        'eat-right': 'Shutters_E02_11_Eat_Right',
        'coffebar': 'Shutters_E03_24_Coffee',
        'kitchen-corner-north': 'Shutters_E03_23_Corner_North',
        'kitchen-corner-east': 'Shutters_E03_22_Corner_East',
        'kitchen-east': 'Shutters_E03_21_East',
        'kitchen-window': 'Shutters_E03_20_Window'
    },
    'slat': {
        'no-location': 'Shutters_E02_E03_All_L',
        'eat-left': 'Shutters_E02_10_Eat_Left_L',
        'eat-right': 'Shutters_E02_11_Eat_Right_L',
        'coffebar': 'Shutters_E03_24_Coffee_L',
        'kitchen-corner-north': 'Shutters_E03_23_Corner_North_L',
        'kitchen-corner-east': 'Shutters_E03_22_Corner_East_L',
        'kitchen-east': 'Shutters_E03_21_East_L',
        'kitchen-window': 'Shutters_E03_20_Window_L'
    }
    
};



// Mode/Scene names to Scene ID mappings
config.mode = {
    'synonyms': {
        'scenes': 'szene',
        'music': 'musik,guten morgen,aufstehen',
        'house_blue': 'haus in blau, blau',
        'house_red': 'haus in rot, rot',
        'house_green': 'haus in grün, grün',
        'house_white': 'haus in weiß, weiß',
        'tv': 'fernsehen,fernseh schauen',
        'cook': 'kochen',
        'eat': 'essen',
        'romance': 'romantik',
        'sauna': 'sauna',
        'sauna1': 'wellness',
        'train': 'training',
        'trainMusic': 'training mit musik, training und musik',
        'cool': 'cooldown',
        //        'light-off': 'alle lichter aus,alle lichter ausschalten',
        'all-off': 'alles aus,alles ausschalten, schlafen,schlafen gehen',
        'music-off': 'musik aus,musik ausschalten',
    },
    'scenes': {
        'all-off': '0',
        'music-off': '110',
        'music': '1',
        'house_blue': '2',
        'house_red': '3',
        'house_green': '4',
        'house_white': '14',
        'tv': '5',
        'cook': '6',
        'eat': '7',
        'romance': '8',
        'sauna': '9',
        'sauna1': '10',
        'train': '11',
        'trainMusic': '12',
        'cool': '13'
    },

    'lighting': {
        'all off': '0',
        'all on': '1',
        'focus': '2',
        'energize': '3',
        'relax': '4',
        'party': '5',
        'night light': '6',
        'bed time': '7',
        'love shack': '8',
        'lava': '9'
    },
    'security': {
        'off': '0',
        'sleep': '1',
        'home': '2',
        'away': '3'
    }
};

// Metric names to Item mappings
config.metric = {
    'synonyms': {
        'temperature': 'temperatur',
        'floor-temp': 'fußbodentemperatur',
        'humidity': 'luftfeuchtigkeit',
        'power': 'energieverbrauch',
        'luminance': 'helligkeit',
    },
    'temperature': {
        'outdoor': 'Quadra_Temperature',
        'no-location': 'Quadra_Temperature',
        'spa': 'Temperature_U02_Spa',
        'sauna': 'Temperature_U01_Sauna',
        'office': 'Temperature_U03_Office',
        'laundry': 'Temperature_U04_Laundry',
        'cinema': 'Temperature_U06_Cinema',
        'technics': 'Temperature_U08_Technics',
        'corridorUG': 'Temperature_U09_Corridor',

        'living': 'Temperature_E01_Living',
        'eat': 'Temperature_E02_Eat',
        'kitchen': 'Temperature_E03_Kitchen',
        'corridorEG': 'Temperature_E04_Corridor',
        'toilet': 'Temperature_E06_Toilet',

        'sleep': 'Temperature_O01_Sleep',
        'bath1': 'Temperature_O02_Bath1',
        'bath2': 'Temperature_O03_Bath2',
        'guest1': 'Temperature_O05_Guest1',
        'guest2': 'Temperature_O04_Guest2',
        'corridorOG': 'Temperature_O06_Corridor',

        'garage': 'Temperature_E05_Garage'
    },
    'humidity': {
        'spa-shower': 'Wiregate_Hum_U02_SpaShower',
        'cinema': 'Wiregate_Hum_U06_Cinema',
        'bath1': 'Wiregate_Hum_O02_Bath1',
        'bath2': 'Wiregate_Hum_O03_Bath2'
    },

    'luminance': {
        'outdoor': 'Quadra_LightnessSky',
        'no-location': 'Quadra_LightnessSky',
        'spa': 'Motion_Lum_U02_Spa',
        'office': 'Motion_Lum_U03_Office',
        'laundry': 'Motion_Lum_U04_Laundry',
        'cinema': 'Motion_Lum_U06_Cinema',
        'corridorUG': 'Motion_Lum_U09_30_Corridor',

        'living': 'Motion_Lum_E01_Living',
        'eat': 'Motion_Lum_E02_30_Eat',
        'kitchen': 'Motion_Lum_E03_Kitchen',
        'corridorEG': 'Motion_Lum_E04_Corridor',
        'toilet': 'Motion_Lum_E06_Toilet',

        'corridorOG': 'Motion_Lum_O06_30_Corridor',

        'entrance': 'Motion_Lum_E08_Entrance'
    },
    'power': {
        'house': 'HEM1_P2',
        default: 'HEM1_P2'
    },
};

// Unit to metric mappings
config.unit = {
    'temperature': 'grad celsius',
    'humidity': 'prozent',
    'luminance': 'lux',
    'power': 'kilowatt'
};

// percent values
config.percent = {
    'null': '0',
    'fünf': '5',
    'zehn': '10',
    'fünfzehn': '15',
    'zwanzig': '20',
    'fünf und zwanzig': '25',
    'dreißig': '30',
    'fünf und dreißig': '35',
    'vierzig': '40',
    'fünf und vierzig': '45',
    'fünfzig': '50',
    'fünf und fünfzig': '55',
    'sechzig': '60',
    'fünf und sechzig': '65',
    'siebzig': '70',
    'fünf und siebzig': '75',
    'achzig': '80',
    'fünf und achzig': '85',
    'neunzig': '90',
    'fünf und neunzig': '95',
    'hundert': '100'
};

// Color name to HSB value mappings
config.color = {
    'weiß': '0,0,100',
    'rot': '0,100,100',
    'orange': '40,100,100',
    'gelb': '60,100,100',
    'grün': '120,100,100',
    'aqua': '180,100,100',
    'blau': '240,100,100',
    'rosa': '290,100,100',
    'magenta': '310,100,100',
    'pink': '330,100,100',
    'schwarz': '0,0,0'
};


/******************* ASK Utterances ************************/
// Configure ASK Intent utterances using Alexa-App - reference:
//  https://www.npmjs.com/package/alexa-app#schema-and-utterances for syntax
config.utterances = {
    'Switch': [
        "schalte {das|die|den|mein|meine |} {-|ItemName} {im|in der|} {-|Location} {-|Action}",
        "schalte {das|die|den|mein|meine |} {-|ItemName} {-|Action}",
    ],
    'BlockMotion': [
        "{-|BlockAction} {den|die | } bewegungsmelder {im|in der|} {-|Location}",
        "schalte {den|die | } bewegungsmelder {im|in der|} {-|Location} {-|BlockAction}",
    ],
    // Dim lights in a particular room
    'SetLevel': [
        "dimme {das|die|den|mein|meine |} {-|ItemName} {im|in der|} {-|Location} auf {-|Percent} {prozent |}",
        "dimme {das|die|den|mein|meine |} {-|ItemName} auf {-|Percent} {prozent |}",
    ],
    // Set HSB color values for lights in a particular room
    'SetColor': [
        "{färbe|setze|ändere|stelle} {die|meine |} {-|ItemName} {im|in der|} {-|Location} auf {die farbe|} {-|Color}",
        // "färbe {das|die|den|mein|meine |} {-|ItemName} {im|in der|} {-|Location} auf {-|Color}",
    ],
    // Set house/lighting/security/etc scenes
    'SetMode': [
        "{ich möchte|wähle|setze} {die|} {-|ModeType} {-|ModeName}",
        "ich möchte {-|ModeName}",
        "{schalte} {die|} {-|ModeType} {-|ModeName} ein",
        "dass ich {das|die|} {-|ModeType} {-|ModeName} möchte",
        "dass ich {das|die|} {-|ModeName} möchte",
    ],
    // Set house/lighting/security/etc scenes
    'GetWindowDoorState': [
        "ob das haus {sicher|verschlossen} ist",
        "ist das haus sicher",
        "nach dem {aktuellen|} Status {der|} {Türen|Fenster|}",
        "nach dem {aktuellen|} {Tür|Fenster|} Status",
    ],
    // Get current item state values
    'GetState': [
        "nach {der|dem} {aktuellen|} {-|MetricName} {im|in der|} {-|Location}",
        "nach {der|dem} {aktuellen|} {-|Location} {-|MetricName}",
        "wie hoch ist {der|dem|die} {aktuelle|} {-|MetricName} {im|in der|} {-|Location}",
        "wie hoch {der|dem|die} {aktuelle|} {-|MetricName} {im|in der|} {-|Location} ist",
    ],
    // Get current item state values
    'Rollershutter': [
        "{-|ShutterAction} die {-|ShutterModeType} {im|in der|an der|bei der|} {-|ShutterLocation}",
        "fahre die {-|ShutterModeType} {im|in der|an der|bei der|} {-|ShutterLocation} {-|ShutterAction}"
    ],
    /*
    // Set target temperature for house HVAC
    'SetTemp': [
        "{to |} {set|change|adjust} {the|my |} {-|Location} {thermostat|temperature} to {60-80|Degree} {degrees |}",
        "{to |} {set|change|adjust} {the|my |} {thermostat|temperature} {in the|in my |} {-|Location} to {60-80|Degree} {degrees |}"
    ],
    // Get current house/lighting/security/etc scene
    'GetMode': [
        "{to |} {get|check} {the|my |} {house|lighting|security|ModeType} mode {set to |}",
        "whats {the|my |} {house|lighting|security|ModeType} mode {set to |}"
    ],
    // Execute 'raw' voice commands, request/response handled entirely by custom HA server rules
    'VoiceCMD': [
        "{lets party|Input}",
        "{call my bank|call my phone|call work|Input}",
        "{status update|Input}",
        "{for a |}{status update|Input}",
        "{whats the |}{weather like|Input}",
        "{say a |} {quote|Input}",
        "{its} {cold in here|Input}",
        "{its} {hot in here|Input}"
    ],
    // Research something arbitrary via Wolfram API call
    'Research': [
        "{to |} research {what is the atomic weight of boron|Question}",
        "{to |} research {what is thirty divided by five|Question}",
        "{to |} research {what did Alexander Graham Bell invent|Question}",
        "{to |} research {what does eleven plus seven equal|Question}",
        "{to |} research {when is sunset tonight|Question}",
        "{to |} research {how tall is mount everest|Question}",
        "{to |} research {how many miles in an acre|Question}",
        "{to |} research {how do you take a derivative|Question}",
        "{to |} research {where did George Washington die|Question}",
        "{to |} research {where are the pyramids|Question}",
        "{to |} research {Why is the sky blue|Question}"
    ],

    // Stop Intent
    'Stop': [
        "{stopp|hör endlich auf|aufhören}"
    ],
    // Cancel Intent
    'Cancel': [
        "{abbrechen|abbreche|vergiss es}"
    ],
    // Help Intent
    'Help': [
        "{hilf|hilfe} {mir |}",
        "Kannst du mir helfen {bitte |}" 

    ]
    */

    //TODO implement the yes/no/repeat intents
    //// Yes Intent
    //'Yes': [
    //    "yes {please |}",
    //    "sure",
    //    "do it",
    //    "affirmative"
    //],
    //// No Intent
    //'No': [
    //    "no {thanks |}",
    //    "nope",
    //    "negative",
    //    "negatory"
    //],
    //// Repeat Intent
    //'Repeat': [
    //    "{please |} repeat {that |}", 
    //    "say that again",
    //    "what did you say"
    //]
};

/******************* ASK Responses *************************/
// Help response & card
config.help = {
    'say': [ 
        'Ich kann dir bei der Steuereung deines Hauses behilflich sein. Sag mir zum Beispiel, Schalte das licht in der küche ein, Dimme das Licht im Esszimmer auf 50 Prozent, ' +
        'Ändere die Farbe des Leuchtbandes auf Grün, setze die Temperatur im Wohnzimmer auf 28 Grad. ' +
        'Ich habe dir einen Spickzettel mit allen möglichen Kommandos auf deine Alexa App geschickt.'
    ],
    'card': [
        'Spickzettel: Schalte das Licht in der Küche ein / Dimme das Licht im Wohnzimmer auf 50% / Ändere die Farbe des Küchenblocks auf Blau' +
        ' / Wie hoch ist die aktuelle Temperatur im Wohnzimmer / Ist das haus sicher ? / Wähle die Szene Sauna / Öffne die Jalousie'
    ]
};

// Exports
module.exports = config;
