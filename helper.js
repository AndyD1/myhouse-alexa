var config = require('./config');

/************** CONFIG HELPER *********************/
function getAction(actionName) {
    for (var key in config.action) {
        if (config.action.hasOwnProperty(actionName)) {
            return config.action[actionName];
        }
    }
    return false;
}

function getRollershutterAction(actionName) {
    actionName = getSynonym(actionName, config.shutters.synonyms);
    for (var key in config.rollershuterAction) {
        if (config.rollershuterAction.hasOwnProperty(actionName)) {
            return config.rollershuterAction[actionName];
        }
    }
    return false;
}

function getBlockAction(actionName) {
    for (var key in config.blockAction) {
        if (config.blockAction.hasOwnProperty(actionName)) {
            return config.blockAction[actionName];
        }
    }
    return false;
}

// get percent values
function getPercent(word) {
    for (var key in config.percent) {
        if (config.percent.hasOwnProperty(word)) {
            return config.percent[word];
        }
    }
    return false;
}

// get synonyms for german words
function getSynonym(syn, cnf) {
    for (var s in cnf) {
        var keys = cnf[s].split(',');
        for (var key of keys) {
            //console.log(`Testing key ='${key}', syn='${syn}'`)
            if (key.trim() === syn.toLowerCase()) {
                return s;
            }
        }
    }
    return syn;
}

function shouldCheckState(itemName) {
   return  config.item.dontCheckState.indexOf(itemName) == -1;
}

// Get itemName from itemType & location
function getItem(itemType, location) {

    itemType = itemType.toLowerCase();
    location = location.toLowerCase();

    itemType = getSynonym(itemType, config.synonyms);
    location = getSynonym(location, config.synonyms);
    
    for (var key in config.item) {
        if (config.item.hasOwnProperty(itemType)) {
            return config.item[itemType][location];
        }
    }
    return false;
}
// Get itemName from mode & location
function getRollershutterItem(mode, location) {

    mode = mode.toLowerCase();
    location = location.toLowerCase();

    mode = getSynonym(mode, config.shutters.synonyms);
    location = getSynonym(location, config.shutters.synonyms);
    
    //var shutterObj = mode === 'shutter' ? config.shutters.shutter : config.shutters.slat
    for (var key in config.shutters) {
        if (config.shutters.hasOwnProperty(mode)) {
            return config.shutters[mode][location];
        }
    }
    return false;
}

// Get itemName from metricName & location
function getMetric(metricNameDE, locationDE) {
    var metricName = getSynonym(metricNameDE, config.metric.synonyms);
    var location = getSynonym(locationDE, config.synonyms);
    
    for (var key in config.metric) {
        if (config.metric.hasOwnProperty(metricName)) {
            return config.metric[metricName][location];
        }
    }
    return false;
}

// Get unit from metricName
function getUnit(metricNameDE) {
    var metricName = getSynonym(metricNameDE, config.metric.synonyms);
    for (var key in config.unit) {
        if (config.unit.hasOwnProperty(metricName)) {
            return config.unit[metricName];
        }
    }
    return false;
}

// Get HSB value from colorName
function getColor(colorName) {
    for (var key in config.color) {
        if (config.color.hasOwnProperty(colorName)) {
            return config.color[colorName];
        }
    }
    return false;
}

// Get itemName from modeType & modeName
function getMode(modeType, modeName) {
    //console.log(`REQUEST: getMode called with : modeType=${modeType}, modeName=${modeName}`);
    modeType = getSynonym(modeType, config.mode.synonyms);
    modeName = getSynonym(modeName, config.mode.synonyms);
    //console.log(`REQUEST: getMode returned => Synonyms : modeType=${modeType}, modeName=${modeName}`);

    for (var key in config.mode) {
        if (config.mode.hasOwnProperty(modeType)) {
            return config.mode[modeType][modeName];
        }
    }
    return false;
}

// Get modeName from modeType & modeId
function getModeName(modeType, modeId) {
    for (var key in config.mode[modeType]) {
        if (config.mode[modeType][key] === modeId) {
            return key;
        }
    }
    return false;
}

/*
 * returns all items in a Openhab Group 
 * 
 * return List of {type, name, state}
 */
function flattenItems(itemArr) {
    var itemList = [];
    for (var m of itemArr) {
        if (m.type === 'Group') {
            itemList = itemList.concat(flattenItems(m.members));
        }
        else {
            itemList.push({ type: m.type, name: m.name, state: m.state });
            //console.log(m.type, m.name, m.state);
        }
    }
    return itemList;
}

function translateItem(itemName) {
   for (var key in config.translate.item) {
        if (config.translate.item.hasOwnProperty(itemName)) {
            return config.translate.item[itemName];
        }
    }
    return '';
}

/*
function formatString(source, ...params) {
    for(var i in params) {
        source = source.replace(new RegExp("\\{" + i + "\\}", "g"), params[i]);
    }
    return source;
}
*/

// function checkLocation(location) {
//   //console.log('result of checkLocation for ' + location + ' is: ' + config.HA_locations.indexOf(location) > -1);
//   return config.HA_locations.indexOf(location) > -1;
// }

// Exports
module.exports.shouldCheckState = shouldCheckState;
module.exports.getPercent = getPercent;
module.exports.getAction = getAction;
module.exports.getRollershutterAction = getRollershutterAction;
module.exports.getBlockAction = getBlockAction;
module.exports.getItem = getItem;
module.exports.getRollershutterItem = getRollershutterItem;
module.exports.getMetric = getMetric;
module.exports.getUnit = getUnit;
module.exports.getColor = getColor;
module.exports.getMode = getMode;
module.exports.getModeName = getModeName;
module.exports.flattenItems = flattenItems;
module.exports.translateItem = translateItem;
//module.exports.checkLocation = checkLocation;