/******************** INCLUDES **********************/
var config = require('./config');
var helper = require('./helper');
var alexa = require('alexa-app');

var wolfram = require('./lib/wolfram');

// Load supporting library depending on the configured Home Automation controller
if (config.HA_name === 'OpenHAB') {
    var HA = require('./lib/openhab');
}
else {
    console.log('FATAL ERROR: the configured HA_name "' + config.HA_name + '" is not currently supported.');
    process.exit(1);
}

// Reloaded by hotswap when code has changed (by alexa-app-server)
module.change_code = 1;

var appName = config.applicationName;

/******************* ASK MAIN ROUTINES **************/
// Define an alexa-app
var app = new alexa.app(appName);
app.launch(function (request, response) {
    // Store the Launch Intent in session, which later keeps the session going for multiple requests/commands
    response.session('launched', 'true');

    response.say(config.greeting);
    if (config.chime) {
        response.say(config.chime);
    }

    response.shouldEndSession(false, config.greetingHelp);
});

app.sessionEnded(function (request, response) {
    response.say(config.sessionEnd);
});

app.messages.NO_INTENT_FOUND = config.noIntentFound;

// Pre-execution security checks - ensure each requests applicationId / userId / password match configured values
app.pre = function (request, response, type) {
    // Extract values from various levels of the nested request object
    var address = request.data.remoteAddress;
    var password = request.data.password;
    var timestamp = request.data.request.timestamp;
    var requestId = request.data.request.requestId;
    var sessionId = request.sessionId;
    var userId = request.sessionDetails.userId;
    var applicationId = request.sessionDetails.application.applicationId;

    // Log the request
    console.log(`${address}-${timestamp}- AWS ASK ${type} received: ${requestId}/${sessionId}`);

    if (applicationId !== config.applicationId) {
        console.log(`${address}-${timestamp}- - ERROR: Invalid application ID in request: ${applicationId}`);
        response.fail("Invalid application ID");
    }
    /*
    if (userId !== config.userId) {
        console.log(address + ' - ' + timestamp + ' - ERROR: Invalid userId in request: ' + userId );
        response.fail("Invalid user ID");
    }
    if (password !== config.password) {
        console.log(address + ' - ' + timestamp + ' - ERROR: Invalid password in request: ' + password);
        response.fail("Invalid password");
    }
    */
};

// Post commands
//app.post = function(request,response,type,exception) {
// Always turn an exception into a successful response, so it can be spoken to the user
//response.clear().say("An error occurred: " + exception).send();
//};

// Generic error handler
app.error = function (exception, request, response) {
    console.log(exception);
    response.say(config.genericError);
};


// Switch devices ON/OFF
app.intent('Switch', {
    "slots": { "Action": "SWITCH_ACTION_TYPE", "ItemName": "ITEM_TYPE", "Location": "LOCATION_TYPE" }
    , "utterances": config.utterances.Switch
}, function (request, response) {
    var actionDE = request.slot('Action');
    var itemName = request.slot('ItemName');
    var locationDE = checkLocation(request.slot('Location'));

    // DEBUG response
    //console.log('RawResponseData: ',request.data);
    console.log(`REQUEST: Switch Intent slots are: ${actionDE}/${itemName}/${locationDE}`);
    if (actionDE && itemName && locationDE) {

        var HA_item = helper.getItem(itemName, locationDE);
        if (HA_item) {
            // translate Action
            var action = helper.getAction(actionDE);

            // check if weshoud check the item state first ?
            if (helper.shouldCheckState(HA_item)) {
                // Get current state
                HA.getState(HA_item, function (err, state) {
                    if (err) {
                        console.log('REQUEST: getState failed:  ' + err.message);
                    }

                    state = checkOnOFFState(state);

                    // Check if the items current state and action match
                    if (state === action) {
                        replyWith(`Dein ${itemName} im ${locationDE} ist bereits ${actionDE}geschaltet`, response);
                    }
                    else {
                        setState(response, HA_item, action, `ok, ich schalte dein ${itemName} im ${locationDE} ${actionDE}`);
                    }
                });
            }
            else {
                setState(response, HA_item, action, `ok, ich schalte dein ${itemName} im ${locationDE} ${actionDE}`);
            }

        }
        // no HA item found
        else {
            console.log(`HA_item for '${itemName}' not found!`);
            replyWith(`Oh Jeh, Ich kann dein ${itemName} im ${locationDE} nicht ${actionDE}schalten`, response);
        }
    }
    // no action
    else if (actionDE == undefined) {
        replyWith(`Mann Oh Mann, du musst mir schon sagen, ob du ein oder aus schalten willst`, response);
    }
    // no item
    else {
        replyWith(`Oh Jeh, Ich weiss leider nicht, was du genau im ${locationDE} schalten möchtest`, response);
    }
    return false;
});

// Set dimming levels of lights
app.intent('SetLevel', {
    "slots": { "Percent": "PERCENT_TYPE", "ItemName": "ITEM_TYPE", "Location": "LOCATION_TYPE" }
    , "utterances": config.utterances.SetLevel
}, function (request, response) {
    var percentDE = request.slot('Percent');
    var itemName = request.slot('ItemName');
    var locationDE = checkLocation(request.slot('Location'));

    // DEBUG response
    //console.log('RawResponseData: ',request.data);
    console.log(`REQUEST: SetLevel Intent slots are: ${percentDE}/${itemName}/${locationDE}`);

    // Handle undefined ASK slots
    if (itemName && locationDE && percentDE) {
        var HA_item = helper.getItem(itemName, locationDE);
        var percent = helper.getPercent(percentDE)
        if (HA_item && percent) {

            if (percent) {
                // Get current color
                HA.getState(HA_item, function (err, state) {
                    if (err) {
                        console.log('HA getState failed:  ' + err.message);
                    }
                    // Check if the current dimmer level and new level match
                    if (state === percent) {
                        replyWith(`Dein ${itemName} im ${locationDE} ist bereits auf ${percent} prozent`, response);
                    }
                    // Set the new state for the item
                    else if (state !== percent) {
                        setState(response, HA_item, percent, `ok, dein ${itemName} im ${locationDE} ist auf ${percent} prozent gesetzt`);
                    }
                });
            }
            else {
                replyWith(`Oh Jeh, dein Dimmwert ist falsch!`, response);
            }

        }
        // no HA item found
        else {
            console.log(`could not find HA_item for '${itemName}'`);
            replyWith(`Momentan kann ich dein ${itemName} im ${locationDE} nicht dimmen`, response);
        }
    }
    else {
        replyWith('Oh Jeh, dieses Gerät kann ich nicht dimmen', response);
        return;
    }
    return false;
});


// Set HSB color for lights
app.intent('SetColor', {
    "slots": { "ItemName": "ITEM_TYPE", "Location": "LOCATION_TYPE", "Color": "COLOR_TYPE" }
    , "utterances": config.utterances.SetColor
}, function (request, response) {
    var itemName = request.slot('ItemName');
    var color = request.slot('Color');
    var locationDE = checkLocation(request.slot('Location'));

    // DEBUG response
    //console.log('RawResponseData: ',request.data);
    console.log(`REQUEST: SetColor Intent slots are: ${color}/${itemName}/${locationDE}`);

    // Handle undefined ASK slots
    if (locationDE && color) {
        var HA_item = helper.getItem(itemName, locationDE);
        var HSBColor = helper.getColor(color);
    }
    else {
        replyWith('Sorry, ich kann diese Farbe nicht einstellen', response);
        return;
    }

    if (color && locationDE && HSBColor && HA_item) {
        // Get current color
        HA.getState(HA_item, function (err, state) {
            if (err) {
                console.log('HA getState failed:  ' + err.message);
            }
            // Check if the current color and new color match
            if (state === HSBColor) {
                replyWith(`Dein ${itemName} im ${locationDE} hat bereits die Farbe ${color}`, response);
            }
            // Set the new state for the item
            else if (state !== HSBColor) {
                setState(response, HA_item, HSBColor, `ok, dein ${itemName} im ${locationDE} ist auf ${color} gesetzt`);
            }
        });
    }
    else {
        replyWith(`Sorry, ich kann die Farbe bei deinem ${itemName} im ${locationDE} nicht einstellen`, response);
    }
    return false;
});

// Switch devices ON/OFF
app.intent('BlockMotion', {
    "slots": { "BlockAction": "BLOCK_ACTION_TYPE", "Location": "LOCATION_TYPE" }
    , "utterances": config.utterances.BlockMotion
}, function (request, response) {
    var actionDE = request.slot('BlockAction');
    var locationDE = checkLocation(request.slot('Location'));

    // DEBUG response
    //console.log('RawResponseData: ',request.data);
    console.log(`REQUEST: BlockMotion Intent slots are: ${actionDE}/${locationDE}`);
    if (actionDE && locationDE) {

        var HA_item = helper.getItem('motion', locationDE);
        if (HA_item) {
            // translate Action
            var action = helper.getBlockAction(actionDE);

            // Get current state
            HA.getState(HA_item, function (err, state) {
                if (err) {
                    console.log('REQUEST:HA getState failed:  ' + err.message);
                }
                // Check if the items current state and action match
                if (state === action) {
                    replyWith(`Dein Bewegungsmelder im ${locationDE} ist bereits ${action === 'ON' ? 'gesperrt' : 'entsperrt'}`, response);
                }
                // Set the new state for the item
                // else if (state !== action) {
                else {
                    setState(response, HA_item, action, `ok, ich ${action === 'ON' ? 'sperre' : 'entsperre'} deinen Bewegungsmelder im ${locationDE}`);
                }
            });
        }
        // no HA item found
        else {
            console.log(`could not find HA_item for '${itemName}'`);
            replyWith(`Oh Jeh, Ich kann deinen Bewegungsmelder im ${locationDE} nicht ${action === 'ON' ? 'sperren' : 'entsperren'}`, response);
        }
    }
    // no action
    else if (actionDE == undefined) {
        replyWith(`Mann Oh Mann!, du musst mir schon sagen, was du genau von mir willst`, response);
    }
    // no item
    else {
        replyWith(`Oh Jeh, Ich weiss leider nicht, was du genau im ${locationDE} möchtest`, response);
    }
    return false;
});



// Set modes (house/lighting/security scenes)
app.intent('SetMode', {
    "slots": { "ModeType": "MODE_TYPE", "ModeName": "MODE_NAME_TYPE" }
    , "utterances": config.utterances.SetMode
}, function (request, response) {
    var modeTypeDE = request.slot('ModeType');
    var modeNameDE = request.slot('ModeName');

    if (modeTypeDE == undefined || modeTypeDE == 'undefined') {
        modeTypeDE = "szene";
    }

    // DEBUG response
    //console.log('RawResponseData: ',request.data);
    console.log(`REQUEST: SetMode Intent slots are: ${modeTypeDE}/${modeNameDE}`);

    if (modeTypeDE && modeNameDE) {
        var modeId = helper.getMode(modeTypeDE, modeNameDE);
        var HA_item = helper.getItem('mode', modeTypeDE);

        if (modeId && HA_item) {
            setState(response, HA_item, modeId, `ok, ich setze die ${modeTypeDE} auf ${modeNameDE}`);
        }
        else {
            replyWith(`Sorry, ich kann deine ${modeTypeDE} nicht auf ${modeNameDE} setzen`, response);
        }
    }
    else {
        replyWith('Sorry, diesen Modus kann ich nicht setzen', response);
    }
    return false;
});

// Set modes (house/lighting/security scenes)
app.intent('GetWindowDoorState', {
    // "slots": {},
    "utterances": config.utterances.GetWindowDoorState
}, function (request, response) {
    // DEBUG response
    //console.log('RawResponseData: ',request.data);
    console.log(`REQUEST: GetWindowDoorState Intent`);

    // get all open windows/doors
    HA.getItem('gWindowsDoorsOpen', function (err, json) {
        if (!err) {
            var open = helper.flattenItems(json.members).filter((itm) => { return itm.type === 'Contact' && itm.state !== 'CLOSED' });

            // no doors open => check ventilation
            if (open.length == 0) {
                // check for doors/windows in ventilation
                HA.getItem('gWindowsDoorsVent', function (err, json) {
                    if (!err) {
                        var ventilation = helper.flattenItems(json.members).filter((itm) => { return itm.type === 'Contact' && itm.state !== 'CLOSED' });

                        // all doors and windows closed !
                        if (ventilation.length == 0) {
                            replyWith("Alles ok! Fenster und Türen sind fest verschlossen.", response);
                        }
                        // only doors and windows in ventilatiuon
                        else {
                            var text = "Alle Fenster sind geschlossen! Aber ";
                            for (var dw of ventilation) {
                                text += helper.translateItem(dw.name) + " ist auf lüftung. ";
                            }
                            replyWith(text, response);
                        }


                    }
                });

            }
            // doors/windows open
            else {
                var text = "Achtung ! "
                for (var dw of open) {
                    text += helper.translateItem(dw.name) + " ist offen. ";
                }
                replyWith(text, response);
            }
        }
    });

    return false;
});

// Check the state of an itemName
app.intent('GetState', {
    "slots": { "MetricName": "METRIC_NAME_TYPE", "Location": "LOCATION_TYPE" }
    , "utterances": config.utterances.GetState
}, function (request, response) {
    var metricNameDE = request.slot('MetricName');
    var locationDE = checkLocation(request.slot('Location'));

    // DEBUG response
    //console.log('RawResponseData: ',request.data);
    console.log(`REQUEST: GetState Intent slots are: ${metricNameDE}/${locationDE}`);

    if (metricNameDE && locationDE) {
        var HA_item = helper.getMetric(metricNameDE, locationDE);
        var HA_unit = helper.getUnit(metricNameDE);
    }
    else {
        replyWith('Sorry, diesen Status kann ich nicht abfragen', response);
        return;
    }

    if (HA_item && HA_unit) {
        HA.getState(HA_item, (err, state) => {
            if (err) {
                replyWith(config.setStateError, response);
            }
            else {
                state = Number(state).toFixed(1).replace('.', ',');
                //console.log("Value = " + state)
                replyWith(`Deine ${metricNameDE} im ${locationDE} beträgt ${state} ${HA_unit}`, response);
            }
        });
    }
    else {
        replyWith(`Sorry, ich kann deine ${metricNameDE} in der ${locationDE} nicht abfragen`, response);
    }
    return false;
});


// Rollershutter
app.intent('Rollershutter', {
    "slots": { "ShutterAction": "ROLLERSHUTTER_ACTION_TYPE", "ShutterModeType": "ROLLERSHUTTER_MODE_TYPE", "ShutterLocation": "ROLLERSHUTTER_LOCATION_TYPE" }
    , "utterances": config.utterances.Rollershutter
}, function (request, response) {
    var actionDE = request.slot('ShutterAction');
    var modeDE = request.slot('ShutterModeType');
    var locationDE = checkLocation(request.slot('ShutterLocation'));

    // DEBUG response
    //console.log('RawResponseData: ',request.data);
    console.log(`REQUEST: Rollershutter Intent slots are: ${actionDE}/${modeDE}/${locationDE}`);
    if (actionDE && modeDE && locationDE) {

        var HA_item = helper.getRollershutterItem(modeDE, locationDE);
        console.log("Found HA item = " + HA_item);
        if (HA_item) {
            // translate Action
            var action = helper.getRollershutterAction(actionDE);
            console.log("got Action = " + action);


            var text = (actionDE==='öffne'||actionDE==='schließe') ? 
            `ok, ich ${actionDE} deine ${modeDE} im ${locationDE} ` :
            `ok, ich fahre deine ${modeDE} im ${locationDE} ${actionDE}`;
            
            //replyWith(text, response);
            setState(response, HA_item, action, text);

/*
            // check if weshoud check the item state first ?
            if (helper.shouldCheckState(HA_item)) {
                // Get current state
                HA.getState(HA_item, function (err, state) {
                    if (err) {
                        console.log('REQUEST: getState failed:  ' + err.message);
                    }

                    state = checkOnOFFState(state);

                    // Check if the items current state and action match
                    if (state === action) {
                        replyWith(`Dein ${itemName} im ${locationDE} ist bereits ${actionDE}geschaltet`, response);
                    }
                    else {
                        setState(response, HA_item, action, `ok, ich schalte dein ${itemName} im ${locationDE} ${actionDE}`);
                    }
                });
            }
            else {
                setState(response, HA_item, action, `ok, ich schalte dein ${itemName} im ${locationDE} ${actionDE}`);
            }
*/
        }
        // no HA item found
        else {
            console.log(`HA_item for '${itemName}' not found!`);
            replyWith(`Oh Jeh, Ich kann dein ${itemName} im ${locationDE} nicht ${actionDE}schalten`, response);
        }
    }
    // no action
    else if (actionDE == undefined) {
        replyWith(`Bitte sage mir, ob du die Jalousie öffnen oder schließen möchtest`, response);
    }
    // no mode
    else {
        replyWith(`Oh Jeh, Ich weiss leider nicht, ob du die Lamelle oder Jalousie im ${locationDE} fahren möchtest`, response);
    }
    return false;
});

/*
// Set thermostat temperatures
app.intent('SetTemp', {
    "slots": { "Degree": "NUMBER", "Location": "LOCATION_TYPE" }
    , "utterances": config.utterances.SetTemp
}, function (request, response) {
    var degree = request.slot('Degree');
    var location = request.slot('Location');

    // DEBUG response
    //console.log('RawResponseData: ',request.data);
    console.log('REQUEST: SetTemp Intent slots are: ' + degree + '/' + location);

    // Handle undefined ASK slots
    if (degree && location) {
        var HA_item = helper.getItem('thermostat', location);
    }
    else {
        replyWith('I cannot set that temperature', response);
        return;
    }

    if (degree && degree > 60 && degree < 80 && HA_item) {
        // Get current temp
        HA.getState(HA_item, function (err, state) {
            if (err) {
                console.log('HA getState failed:  ' + err.message);
            }
            // Check if the current target temp and new target temp match
            if (state === degree) {
                replyWith('Your ' + location + ' target temperature is already set to ' + degree + ' degrees', response);
            }
            // Set the new state for the item
            else if (state !== degree) {
                HA.setState(HA_item, degree);
                replyWith('Setting your ' + location + ' target temperature to ' + degree + ' degrees', response);
            }
            // Unidentified item
            else {
                replyWith('I could not set your ' + location + ' to ' + degree + ' degrees.  Try something between 60 and 80 degrees fahrenheit.', response);
            }
        });
    }
    else {
        replyWith('I cannot currently set your ' + location + ' temperature to ' + degree + ' degrees', response);
    }
    return false;
});



// Check the state of an itemName
app.intent('GetState', {
    "slots": { "MetricName": "LITERAL", "Location": "LOCATION_TYPE" }
    , "utterances": config.utterances.GetState
}, function (request, response) {
    var metricName = request.slot('MetricName');
    var location = request.slot('Location');

    if (typeof (location) === "undefined" || location === null) { location = 'house'; }

    // DEBUG response
    //console.log('RawResponseData: ',request.data);
    console.log('REQUEST: GetState Intent slots are: ' + metricName + '/' + location);

    if (metricName && location) {
        var HA_item = helper.getMetric(metricName, location);
        var HA_unit = helper.getUnit(metricName);
    }
    else {
        replyWith('I cannot get that devices state', response);
        return;
    }

    if (HA_item && HA_unit) {
        HA.getState(HA_item, function (err, state) {
            if (err) {
                console.log('HA getState failed:  ' + err.message);
            } else if (state) {
                replyWith('Your ' + location + ' ' + metricName + ' is currently ' + state + ' ' + HA_unit, response);
            }
        });
    }
    else {
        replyWith('I cannot currently get the ' + metricName + ' in the ' + location, response);
    }
    return false;
});

// Get current mode (house/lighting/security scenes)
app.intent('GetMode', {
    "slots": { "ModeType": "LITERAL" }
    , "utterances": config.utterances.GetMode
}, function (request, response) {
    var modeType = request.slot('ModeType');

    // DEBUG response
    //console.log('RawResponseData: ',request.data);
    console.log('REQUEST: GetMode Intent slots are: ' + modeType);

    if (modeType) {
        var HA_item = helper.getItem('mode', modeType);

        if (!HA_item) {
            replyWith('I could not get the ' + modeType + ' mode', response);
        }

        HA.getState(HA_item, function (err, modeId) {
            if (err) {
                console.log('HA getState failed:  ' + err.message);
            } else if (modeId) {
                var modeName = helper.getModeName(modeType, modeId);
                replyWith('Your ' + modeType + ' mode is set to ' + modeName, response);
            }
        });
    }
    else {
        replyWith('I cannot currently get the ' + modeType + ' mode', response);
    }
    return false;
});

// Handle arbitrary voice commands, passed to HA VoiceCommand item which then executes server side rules
app.intent('VoiceCMD', {
    "slots": { "Input": "LITERAL" }
    , "utterances": config.utterances.VoiceCMD
}, function (request, response) {
    var voiceCMD = request.slot('Input');

    // DEBUG response
    //console.log('RawResponseData: ',request.data);
    console.log('REQUEST: VoiceCMD Intent slots are: ' + voiceCMD);

    HA.setState(config.HA_item_processed, 'OFF');
    HA.setState(config.HA_item_voicecmd, voiceCMD);

    HA.runVoiceCMD(function (err, msg) {
        if (err) {
            replyWith('I could not reach your Home Automation controller', response);
        } else if (msg) {
            replyWith(msg, response);
        }
    });
    return false;
});

// Research almost anything via wolfram alpha (API Key required)
app.intent('Research', {
    "slots": { "Question": "LITERAL" }
    , "utterances": config.utterances.Research
}, function (request, response) {
    var question = request.slot('Question');

    // DEBUG response
    //console.log('RawResponseData: ',request.data);
    console.log('REQUEST: Research Intent hit! Question is: ' + question);

    // Handle request/response/error from Wolfram
    wolfram.askWolfram(question, function (err, msg) {
        if (err) {
            replyWith('I could not quickly determine an answer to your question', response);
        } else if (msg) {
            replyWith(msg, response);
        }
    });
    return false;
});
*/
app.intent('AMAZON.StopIntent',
    {
        //  "utterances": config.utterances.Stop
    }, function (request, response) {
        console.log('REQUEST:  Stopping...');
        response.say("Servus, machs gut.").send();
    });

app.intent('AMAZON.CancelIntent',
    {
        // "utterances": config.utterances.Cancel
    }, function (request, response) {
        console.log('REQUEST:  Cancelling...');
        response.say("Servus, machs gut").send();
    });

app.intent('AMAZON.HelpIntent',
    {
        //  "utterances": config.utterances.Help
    }, function (request, response) {
        console.log('REQUEST:  Help...');
        response.say(config.help.say.toString()).reprompt('Was möchtest du machen?').shouldEndSession(false);
        response.card(appName, config.help.card.toString());
    });


function setState(response, itemName, state, speechOutput) {
    HA.setState(itemName, state, (err, statusCode) => {
        if (err) {
            replyWith(config.setStateError, response);
        }
        else {
            replyWith(speechOutput, response);
        }
    });
}

function getState(response, itemName, speechOutput) {
    HA.getState(itemName, (err, statusCode) => {
        if (err) {
            replyWith(config.setStateError, response);
        }
        else {
            replyWith(speechOutput, response);
        }
    });
}



//TODO implement the yes/no/repeat intents
//app.intent('YesIntent',
//    {"utterances":config.utterances.Yes
//    },function(request,response) {
//        response.say('I heard you say yes, this has not been implemented yet');
//    });
//
//app.intent('NoIntent',
//    {"utterances":config.utterances.No
//    },function(request,response) {
//        response.say('I heard you say no, this has not been implemented yet');
//    });
//
//app.intent('RepeatIntent',
//    {"utterances":config.utterances.Repeat
//    },function(request,response) {
//        response.say('I heard you say repeat, this has not been implemented yet');
//    });

// Custom ASK response handler
function replyWith(speechOutput, response) {
    // Log the response to console
    console.log('RESPONSE: ' + speechOutput);

    // 'Say' the response on the ECHO
    response.say(speechOutput);

    // Show a 'Card' in the Alexa App
    response.card(appName, speechOutput);

    // If this is a Launch request, do not end session and handle multiple commands
    if (response.session('launched') === 'true') {
        response.shouldEndSession(false);
    }

    // 'Send' the response to end upstream asynchronous requests
    response.send();
}

function checkOnOFFState(state) {
    var number = parseInt(state);
    if (isNaN(number)) {
        return state;
    }
    else {
        return number > 0 ? "ON" : "OFF";
    }
}

function checkLocation(location) {
    if (location == null || location == undefined || typeof (location) === "undefined") {
        location = "haus";
    }
    return location;
}

// TODO - Alexa-HA still needs tested as an AWS Lambda function!
// Connect the alexa-app to AWS Lambda
//exports.handler = app.lambda();

module.exports = app;